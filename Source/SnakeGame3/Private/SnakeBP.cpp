// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBP.h"
#include "SnakeElementBase.h"
#include "Interactable.h"


// Sets default values
ASnakeBP::ASnakeBP()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MovementSpeed = 10;
	LastMoveDirection = EMovementDirection::UP;
}

// Called when the game starts or when spawned
void ASnakeBP::BeginPlay()
{
	Super::BeginPlay();
	//GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, GetActorTransform());
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(4);
	
}

// Called every frame
void ASnakeBP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(DeltaTime);

}

void ASnakeBP::AddSnakeElement(int ElementsNum)
{
	
	for (int i = 0; i < ElementsNum; ++i) {

		FVector NewLocation(SnakeElemArray.Num() * -ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);

		SnakeElemArray.Add(NewSnakeElement);
		NewSnakeElement->SnakeOwner = this;
		NewSnakeElement->SetActorHiddenInGame(true);
		if (i == 0 && HasFirstElement == false)
		{
			
			NewSnakeElement->SetFirstTypeElement();
			HasFirstElement = true;
		}

	}

}

	void ASnakeBP::Move(float DeltaTime)
{

	FVector MovementVector = FVector(ForceInitToZero);
	/*switch (LastMoveDirection) {
	case EMovementDirection::UP:
		MovementVector.X += 90;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= 90;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += 90;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= 90;
		break;
	}*/

	//AddActorWorldOffset(MovementVector);

	SnakeElemArray[0]->ToggleCollision();
	FVector PredecessorLocation;
	for (int i = 0; i < SnakeElemArray.Num(); i++) {
		if (i == 0) {
			if (LastMoveDirection == EMovementDirection::UP) {
				FVector NewFirstLocation(ElementSize, 0, 0);
				PredecessorLocation = SnakeElemArray[i]->GetActorLocation();
				SnakeElemArray[i]->SetActorLocation(NewFirstLocation + SnakeElemArray[i]->GetActorLocation());
			}
			else if(LastMoveDirection == EMovementDirection::DOWN) {
				FVector NewFirstLocation(-ElementSize, 0, 0);
				PredecessorLocation = SnakeElemArray[i]->GetActorLocation();
				SnakeElemArray[i]->SetActorLocation(NewFirstLocation + SnakeElemArray[i]->GetActorLocation());
			}
			else if(LastMoveDirection == EMovementDirection::LEFT) {
				FVector NewFirstLocation(0, ElementSize, 0);
				PredecessorLocation = SnakeElemArray[i]->GetActorLocation();
				SnakeElemArray[i]->SetActorLocation(NewFirstLocation + SnakeElemArray[i]->GetActorLocation());
			}
			else if(LastMoveDirection == EMovementDirection::RIGHT) {
				FVector NewFirstLocation(0, -ElementSize, 0);
				PredecessorLocation = SnakeElemArray[i]->GetActorLocation();
				SnakeElemArray[i]->SetActorLocation(NewFirstLocation + SnakeElemArray[i]->GetActorLocation());
			}
		}
		else {
			FVector NewLocation(PredecessorLocation);
			PredecessorLocation = SnakeElemArray[i]->GetActorLocation();
			SnakeElemArray[i]->SetActorLocation(NewLocation);
		}
		SnakeElemArray[i]->SetActorHiddenInGame(false);
	}
	SnakeElemArray[0]->AddActorWorldOffset(MovementVector);
	SnakeCreated = true;
	

	SnakeElemArray[0]->ToggleCollision();
	CanTurn = true;
}

	void ASnakeBP::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
	{
		if (IsValid(OverlappedElement)) {
			int32 ElemIndex;

			SnakeElemArray.Find(OverlappedElement, ElemIndex);
			bool bIsFirst = ElemIndex == 0;
			IInteractable* InteractableInterface = Cast<IInteractable>(Other);
	
			if (InteractableInterface) {
					InteractableInterface->Interact(this, bIsFirst);
			}
				
		}		
			
	}


	void ASnakeBP::SnakeElementHit(ASnakeElementBase* OverlappedElement, AActor* Other) {
		this->Destroy();
	}






