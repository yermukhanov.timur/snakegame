// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBP.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	//Super::Tick(DeltaTime);
	Super::Tick(1);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent-> BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalComponent);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalComponent);

}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBP>(SnakeActorClass, FTransform());
}

void APlayerPawnBase::HandlePlayerVerticalComponent(float value)
{
	
	if (IsValid(SnakeActor)) {
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::DOWN) {
			if (SnakeActor->CanTurn == true) {
					SnakeActor->LastMoveDirection = EMovementDirection::UP;
					SnakeActor->CanTurn = false;
			}
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::UP) {
			if (SnakeActor->CanTurn == true) {
				SnakeActor->LastMoveDirection = EMovementDirection::DOWN;
				SnakeActor->CanTurn = false;
		    }
		}
	}
}


void APlayerPawnBase::HandlePlayerHorizontalComponent(float value)
{

	if (IsValid(SnakeActor)) {
		if (value > 0 && SnakeActor->LastMoveDirection != EMovementDirection::LEFT) {
			if (SnakeActor->CanTurn == true) {
				SnakeActor->LastMoveDirection = EMovementDirection::RIGHT;
				SnakeActor->CanTurn = false;
			}
		}
		else if (value < 0 && SnakeActor->LastMoveDirection != EMovementDirection::RIGHT) {
			if (SnakeActor->CanTurn == true) {
					SnakeActor->LastMoveDirection = EMovementDirection::LEFT;
					SnakeActor->CanTurn = false;
		    }
		}
	}
}

