// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBP.generated.h"


class ASnakeElementBase;

UENUM()
enum class EMovementDirection {
	UP,
	DOWN,
	RIGHT,
	LEFT
};

UCLASS()
class SNAKEGAME3_API ASnakeBP : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBP ();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;
	
	UPROPERTY(EditDefaultsOnly)
	float ElementSize;
	
	UPROPERTY ()
	TArray <ASnakeElementBase *> SnakeElemArray;

	UPROPERTY()
	bool HasFirstElement = false;

	UPROPERTY ()
	EMovementDirection LastMoveDirection;

	UPROPERTY (EditDefaultsOnly)
	float MovementSpeed;

	UPROPERTY(EditDefaultsOnly)
	bool CanTurn;

	UPROPERTY(EditDefaultsOnly)
	bool SnakeCreated = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void AddSnakeElement(int ElementsNum = 1);


	void Move(float DeltaTime);

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase * OverlappedElement, AActor* OtherActor);

	UFUNCTION()
	void SnakeElementHit(ASnakeElementBase* OverlappedElement, AActor* OtherActor);

};
